First, some variable defintions:
- DISTRO_NAME and DISTRO_VERSION are what is used as the name of the
  Dockerfiles, so for example "fedora" or "ubuntu" and "rawhide", "latest",
  "devel", "latest" or "lts"
- CI_JOB_TOKEN is what you got when creating an access token in Gitlab, so you
  need to be a maintainer for now
- ARCH is the architecture you're building for, so for example x86_64
- BUILD_IMAGE_REGISTRY is "registry.freedesktop.org/cangjie/build-essential",
  unless you deployed your own Gitlab and imported the projects in it

In case you want to run locally the exact script of the CI, here it is
(just remember to `export` all the env vars mentionned above first):

Here's an example of the commands to run in order to reproduce the CI locally:

```
export BUILD_IMAGE_REGISTRY=registry.freedesktop.org/cangjie/build-essential
export ARCH=...
export DISTRO_NAME=...
export DISTRO_VERSION=...
export CI_JOB_TOKEN=...
export BASE_IMAGE=${BUILD_IMAGE_REGISTRY}/${ARCH}/${DISTRO_NAME}:${DISTRO_VERSION}
export IMAGE=${BUILD_IMAGE_REGISTRY}/${ARCH}/${DISTRO_NAME}:${DISTRO_VERSION}
buildah login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${BUILD_IMAGE_REGISTRY}
buildah build --build-arg BASE_IMAGE=${BASE_IMAGE} --pull -f Dockerfile.${DISTRO_NAME}-${DISTRO_VERSION} -t ${IMAGE} .
```
