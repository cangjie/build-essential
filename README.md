# Cangjie Build Essential

This provides stable and reproducible build and test environments for the
Cangjians stack.

Feel free to do anything with these images, although we won't provide any
support.

The Cangjie stack currently consists of 3 components:

* [libcangjie]:
  a C library to support the [Cangjie input method][cangjie-wiki];

* [pycangjie]:
  Python bindings to libcangjie built with [Cython];

* [ibus-cangjie]:
  an [IBus][ibus-wiki] engine to implement the Cangjie input method through
  libcangjie;

For more details, please visit our [website].

[libcangjie]: https://gitlab.freedesktop.org/cangjie/libcangjie
[pycangjie]: https://github.com/Cangjians/pycangjie
[ibus-cangjie]: https://github.com/Cangjians/ibus-cangjie
[Cython]: http://cython.org/
[cangjie-wiki]: https://en.wikipedia.org/wiki/Cangjie_input_method
[ibus-wiki]: https://github.com/ibus/ibus/wiki
[website]: https://cangjians.github.io


## Contributing

Please read our [contribution guidelines](CONTRIBUTING.md) for details.


## License

This project is offered under the terms of the
[GNU General Public License, version 3 or any later version][gpl], see
the [COPYING](COPYING) file for details.

[gpl]: http://www.gnu.org/licenses/gpl.html
